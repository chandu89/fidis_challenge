# README

## Please don’t use company mail Id, As DB I can see so use random mail ID eg. gmail, hotmail etc.

#### 1. Sign Up
``` 
curl -X POST \
  https://boiling-island-71155.herokuapp.com//api/v1/users \
  -H 'cache-control: no-cache' \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -H 'postman-token: 689f3f82-c865-804b-1930-4397b1100b2b' \
  -F email=chandanwtb2008@gmail.com \
  -F 'password=1234$aA8' \
  -F 'confirm_password=1234$aA8'
```

#### 2. Authentication API

```
curl -X POST \
  https://boiling-island-71155.herokuapp.com/api/v1/user_sessions \
  -H 'cache-control: no-cache' \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -H 'postman-token: db1540f2-fdb6-c7d6-2f4a-6ef2eca7c331' \
  -F email=chandanwtb2008@gmail.com \
  -F 'password=1234$aA8'

```

#### 3. Upload file API

```

curl -X PUT \
  https://boiling-island-71155.herokuapp.com/api/v1/users/upload_images/ \
  -H 'authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1OTA4NjAxMjN9.bmVOdyJVN344Jqr5c3CvXdKSgMGW37p1lltHI4mP6G8' \
  -H 'cache-control: no-cache' \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -H 'postman-token: 059d7449-2b26-db82-bf21-134586741085' \
  -F 'images=@download (1).jpeg'

```

#### 4. Get Files API

```
curl -X GET \
  https://boiling-island-71155.herokuapp.com/api/v1/users/get_images \
  -H 'authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1OTA4NjAxMjN9.bmVOdyJVN344Jqr5c3CvXdKSgMGW37p1lltHI4mP6G8' \
  -H 'cache-control: no-cache' \
  -H 'postman-token: 57e51efb-b079-fcc3-2c42-1b0cdb5d1fa8'
```



